import React from 'react';
import './scss/App.scss';
import { Header } from './components/Header';
import { Main } from './components/Main';
import { useAppSelector } from './redux/hooks';
import { GalleryPopup } from './components/GalleryPopup';
import classNames from 'classnames';

function App() {
  const popupState = useAppSelector((state) => state.gallery.popupState);

  return (
    <div
      className={classNames('wrapper', {
        active: popupState === true,
      })}>
      <Header />
      <Main />
      {popupState && <GalleryPopup />}
    </div>
  );
}

export default App;
