import React from 'react';

import s from './Cart.module.scss';
import classNames from 'classnames';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';

import picture from '../../images/image-product-1.jpg';
import { clearItem } from '../../redux/cart/slice';

interface ICart {
  cartState: boolean;
}

export const Cart: React.FC<ICart> = ({ cartState }) => {
  const items = useAppSelector((state) => state.cart.items);

  const dispatch = useAppDispatch();

  const deleteItem = () => {
    dispatch(clearItem(items[0].id));
  };

  return (
    <div
      className={classNames(s.cart, {
        [s.active]: cartState === true,
      })}>
      <h4 className={s.cart_title}>Cart</h4>
      <div className={s.cart_body}>
        {items.length > 0 ? (
          <>
            <div className={s.cart_body_item}>
              <div className={s.cart_body_item__img}>
                <img src={picture} alt="" />
              </div>
              <div className={s.cart_body_item__info}>
                <p>Fall Limited Edition Sneakers</p>
                <p>
                  &#36;{items[0].price}.00 x {items[0].count}{' '}
                  <strong> &#36;{items[0].price * items[0].count}.00</strong>
                </p>
              </div>
              <svg
                width="14"
                height="16"
                className={s.cart_body_item__trash}
                onClick={deleteItem}
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink">
                <defs>
                  <path
                    d="M0 2.625V1.75C0 1.334.334 1 .75 1h3.5l.294-.584A.741.741 0 0 1 5.213 0h3.571a.75.75 0 0 1 .672.416L9.75 1h3.5c.416 0 .75.334.75.75v.875a.376.376 0 0 1-.375.375H.375A.376.376 0 0 1 0 2.625Zm13 1.75V14.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 1 14.5V4.375C1 4.169 1.169 4 1.375 4h11.25c.206 0 .375.169.375.375ZM4.5 6.5c0-.275-.225-.5-.5-.5s-.5.225-.5.5v7c0 .275.225.5.5.5s.5-.225.5-.5v-7Zm3 0c0-.275-.225-.5-.5-.5s-.5.225-.5.5v7c0 .275.225.5.5.5s.5-.225.5-.5v-7Zm3 0c0-.275-.225-.5-.5-.5s-.5.225-.5.5v7c0 .275.225.5.5.5s.5-.225.5-.5v-7Z"
                    id="a"
                  />
                </defs>
                <use fill="#C3CAD9" fill-rule="nonzero" xlinkHref="#a" />
              </svg>
            </div>
            <button className={s.cart_body__btn}>Checkout</button>
          </>
        ) : (
          <p>You cart is empty.</p>
        )}
      </div>
    </div>
  );
};
