import React from 'react';

import picture1 from '../../images/image-product-1.jpg';
import picture2 from '../../images/image-product-2.jpg';
import picture3 from '../../images/image-product-3.jpg';
import picture4 from '../../images/image-product-4.jpg';
import { ibg } from '../../utils/ibg';
import { useAppDispatch } from '../../redux/hooks';
import { handlePopup } from '../../redux/gallery/slice';
import classNames from 'classnames';

const arr = [
  {
    id: 1,
    picture: picture1,
  },
  {
    id: 2,
    picture: picture2,
  },
  {
    id: 3,
    picture: picture3,
  },
  {
    id: 4,
    picture: picture4,
  },
] as { id: number; picture: string }[];

export const GalleryPopup: React.FC = () => {
  const [currentImg, setCurrentImg] = React.useState(0);

  const dispatch = useAppDispatch();

  React.useEffect(() => {
    ibg();
  }, []);

  const closePopup = () => {
    dispatch(handlePopup(false));
  };

  const chooseImg = (num: number) => {
    setCurrentImg(num);
  };

  const moveToRight = () => {
    if (currentImg === 3) return;
    setCurrentImg((prev) => prev + 1);
  };
  const moveToLeft = () => {
    if (currentImg === 0) return;
    setCurrentImg((prev) => prev - 1);
  };

  return (
    <div className="popup">
      <div className="popup-close" onClick={closePopup}>
        <svg width="14" height="15" xmlns="http://www.w3.org/2000/svg">
          <path
            d="m11.596.782 2.122 2.122L9.12 7.499l4.597 4.597-2.122 2.122L7 9.62l-4.595 4.597-2.122-2.122L4.878 7.5.282 2.904 2.404.782l4.595 4.596L11.596.782Z"
            fill="#69707D"
            fillRule="evenodd"
          />
        </svg>
      </div>

      <div className="popup-mainImg">
        <div className="popup-mainImg_prev" onClick={moveToLeft}>
          <svg width="12" height="18" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M11 1 3 9l8 8"
              stroke="#1D2026"
              strokeWidth="3"
              fill="none"
              fillRule="evenodd"
            />
          </svg>
        </div>

        <img src={arr[currentImg].picture} alt="" />
        <div className="popup-mainImg_next" onClick={moveToRight}>
          <svg width="13" height="18" xmlns="http://www.w3.org/2000/svg">
            <path
              d="m2 1 8 8-8 8"
              stroke="#1D2026"
              strokeWidth="3"
              fill="none"
              fillRule="evenodd"
            />
          </svg>
        </div>
      </div>
      <div className="popup-row">
        {arr.map((el, idx) => (
          <div
            className={classNames('popup-row__item ibg', {
              currentActiveImg: idx === currentImg,
            })}
            onClick={() => chooseImg(idx)}>
            <img src={el.picture} alt="" />
          </div>
        ))}
      </div>
    </div>
  );
};
