import React from 'react';

import profile from '../../images/image-avatar.png';
import cart from '../../images/icon-cart.svg';

import classNames from 'classnames';
import { Cart } from '../Cart';
import { useAppSelector } from '../../redux/hooks';

export const Header: React.FC = () => {
  const [burgerState, setBurgerState] = React.useState(false);
  const [cartState, setCartState] = React.useState(false);

  const items = useAppSelector((state) => state.cart.items);

  const handleCart = () => {
    setCartState(!cartState);
  };

  return (
    <header className="header">
      <div className="container">
        <div className="header-row">
          <div
            onClick={() => setBurgerState(!burgerState)}
            className={classNames('header-burger', {
              active: burgerState === true,
            })}>
            <span></span>
          </div>
          <h1 className="header-row__title">sneakers</h1>
          <nav
            className={classNames('header-row__menu', {
              active: burgerState === true,
            })}>
            <ul>
              <li>
                <a href="#">Collections</a>
              </li>
              <li>
                <a href="#">Men</a>
              </li>
              <li>
                <a href="#">Women</a>
              </li>
              <li>
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
          <div className="header-row_info">
            <div
              className="header-row_info_cart"
              data-count={items.length > 0 ? items.reduce((acc, el) => acc + el.count, 0) : null}>
              <img src={cart} alt="" onClick={handleCart} />

              <Cart cartState={cartState} />
            </div>

            <img className="header-row_info_avatar" src={profile} alt="" />
          </div>
        </div>
      </div>
    </header>
  );
};
