import React from 'react';
import { MainGallery } from '../MainGallery';
import { MainDesc } from '../MainDesc';

export const Main: React.FC = () => {
  return (
    <main className="main">
      <div className="main-row">
        <MainGallery />
        <MainDesc />
      </div>
    </main>
  );
};
