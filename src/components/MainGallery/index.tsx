import React from 'react';
import s from './MainGallery.module.scss';

import picture1 from '../../images/image-product-1.jpg';
import picture2 from '../../images/image-product-2.jpg';
import picture3 from '../../images/image-product-3.jpg';
import picture4 from '../../images/image-product-4.jpg';
import { ibg } from '../../utils/ibg';
import { useAppDispatch } from '../../redux/hooks';
import { handlePopup } from '../../redux/gallery/slice';

const arr = [
  {
    id: 1,
    picture: picture1,
  },
  {
    id: 2,
    picture: picture2,
  },
  {
    id: 3,
    picture: picture3,
  },
  {
    id: 4,
    picture: picture4,
  },
] as { id: number; picture: string }[];

export const MainGallery: React.FC = () => {
  const [currentImg, setCurrentImg] = React.useState(0);
  const [showArrows, setShowArrows] = React.useState(false);
  const [arrPicture, setArrPicture] = React.useState<{ id: number; picture: string }[]>([
    {
      id: 1,
      picture: picture1,
    },
    {
      id: 2,
      picture: picture2,
    },
    {
      id: 3,
      picture: picture3,
    },
    {
      id: 4,
      picture: picture4,
    },
  ]);
  const dispatch = useAppDispatch();

  const checkResize = () => {
    if (window.innerWidth <= 800) {
      setShowArrows(true);
    }
  };

  React.useEffect(() => {
    ibg();
  }, []);

  React.useEffect(() => {
    window.addEventListener('resize', checkResize);

    return () => {
      window.removeEventListener('resize', checkResize);
    };
  });

  const selectPicture = (event: any) => {
    if (window.matchMedia('(min-width: 801px)').matches) {
      const target = event.target.children[0].tagName;
      if (target !== 'IMG') return;
      dispatch(handlePopup(true));
    }
  };
  const moveToRight = () => {
    console.log(currentImg);
    if (currentImg === 3) return;
    setCurrentImg((prev) => prev + 1);
  };
  const moveToLeft = () => {
    if (currentImg === 0) return;
    setCurrentImg((prev) => prev - 1);
  };
  return (
    <div className={s.gallery} onClick={selectPicture}>
      <div className={s.gallery_main + ' ' + 'ibg'}>
        {showArrows && (
          <div className={s.gallery_main_prev + ' ' + 'popup-mainImg_prev'} onClick={moveToLeft}>
            <svg width="12" height="18" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M11 1 3 9l8 8"
                stroke="#1D2026"
                strokeWidth="3"
                fill="none"
                fillRule="evenodd"
              />
            </svg>
          </div>
        )}

        <img src={arrPicture[currentImg].picture} alt="" />

        {/* {arr.map((el, idx) => (
          // <img src={idx === currentImg && arr[currentImg].picture} alt="" />
        ))} */}
        {showArrows && (
          <div className={s.gallery_main_next + ' ' + 'popup-mainImg_next'} onClick={moveToRight}>
            <svg width="13" height="18" xmlns="http://www.w3.org/2000/svg">
              <path
                d="m2 1 8 8-8 8"
                stroke="#1D2026"
                strokeWidth="3"
                fill="none"
                fillRule="evenodd"
              />
            </svg>
          </div>
        )}
      </div>
      <div className={s.gallery_row}>
        <div className={s.gallery_row_item + ' ' + 'ibg' + ' ' + s.currentImg}>
          <img src={picture1} alt="" />
        </div>
        <div className={s.gallery_row_item + ' ' + 'ibg'}>
          <img src={picture2} alt="" />
        </div>
        <div className={s.gallery_row_item + ' ' + 'ibg'}>
          <img src={picture3} alt="" />
        </div>
        <div className={s.gallery_row_item + ' ' + 'ibg'}>
          <img src={picture4} alt="" />
        </div>
      </div>
    </div>
  );
};
