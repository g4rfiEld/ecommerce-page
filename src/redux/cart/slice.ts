import {createSlice} from '@reduxjs/toolkit'
import type {PayloadAction} from '@reduxjs/toolkit'
import { ICart } from './types'

type PropsType = {
  id: number,
  name: string,
  price: number,
  count: number
}

type Props = {
  items: Array<PropsType>
}

const initialState: Props = {
  items: [] 
}

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem: (state, action: PayloadAction<PropsType>) => {
      const findItem = state.items.find(el => el.id === action.payload.id)

      if(findItem){
        findItem.count += action.payload.count
      }else{
        state.items.push({...action.payload, count: action.payload.count})
      }

    },
    clearItem: (state, action: PayloadAction<number>) => {
      state.items = state.items.filter(el => el.id !== action.payload)

    }
  }
})

export const {addItem, clearItem} = cartSlice.actions

export default cartSlice.reducer