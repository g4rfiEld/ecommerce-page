import {createSlice} from '@reduxjs/toolkit'
import type {PayloadAction} from '@reduxjs/toolkit'


type PropsGallery = {
  popupState: boolean
}

const initialState: PropsGallery = {
  popupState: false
}

export const gallerySlice = createSlice({
  name: 'gallery',
  initialState,
  reducers: {
    handlePopup: (state, action: PayloadAction<boolean>) => {
      state.popupState = action.payload
    }
  }
})

export const {handlePopup} = gallerySlice.actions

export default gallerySlice.reducer