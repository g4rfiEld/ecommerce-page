

import { configureStore } from "@reduxjs/toolkit";
import  cartSlice  from "./cart/slice";
import gallerySlice from "./gallery/slice";


export const store = configureStore({
  reducer: {
    cart: cartSlice,
    gallery: gallerySlice
  }
})

export type RootState  = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch